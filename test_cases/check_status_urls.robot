*** Settings ***
Library     RequestsLibrary
Library     String
Resource    ./locators/page_elements.robot

*** Variables ***
# Replace with your url sitemap
${base_url}     https://www.dktp.nl/sitemap_index.xml
# Use for cross browser testing
@{list_of_browsers}     chrome  firefox     Edge

*** Test Cases ***
Get All Links
    FOR     ${element}  IN  @{list_of_browsers}
    run keyword if  '${element}'=='Edge'     run keyword     open browser    ${base_url}     ${element}    options=use_chromium=True
    ...     ELSE    open browser    ${base_url}     ${element}
    maximize browser window
    Go To Through All Links
    END
    close browser

*** Keywords ***
Go To Through All Links
    ${CountAllLinks}=   get element count   xpath://tr/td/a
    log to console  ${CountAllLinks}
    FOR   ${i}    IN RANGE    1   ${CountAllLinks}+1
    ${sitemap_link}=    get text    xpath:(//tr/td/a[contains(@href, '')])[${i}]
    click link   ${sitemap_link}
    Check Status of All Links
    Go Back
    END