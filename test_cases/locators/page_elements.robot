*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${url}  https://www.dktp.nl/  # Replace with your url

*** Keywords ***
Check Status of All Links
    # Count all links
    ${AllLinksCount}=   get element count   xpath://tr/td/a
    log to console  ${AllLinksCount}
    # Create empty list
    @{LinkItems}    create list
    FOR   ${i}    IN RANGE    1   ${AllLinksCount}+1
    # Append value to list
    ${linkText}=    get text    xpath:(//tr/td/a[contains(@href, '')])[${i}]
    ${remove_strings}=      remove string   ${linkText}     ${url}
    # Make a HTTP session and compare with given response status
    create session  FetchData   ${url}      verify=true
    ${Response}=    get request     FetchData   ${remove_strings}
    ${actual_code}=     convert to string   ${Response.status_code}
    run keyword if  '${actual_code}'=='200'     run keyword     Success
    ...     ELSE    Failure
    END

Go Back to ${url}
    Execute Javascript  history.back()
    wait until page contains element    ${base_url}

Success
    ${passed}=  run keyword and return status   should be equal     ${actual_code}  200
    log     Status code is 200

Failure
    Run Keyword And Continue On Failure    Run Keyword Unless    '${actual_code}'!='200'   Fail    Incorrect code
