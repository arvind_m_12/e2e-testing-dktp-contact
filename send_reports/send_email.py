import smtplib
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import os
from datetime import date


dir_path = "C:/Users/ArvindMehairjan/PycharmProjects/e2e-testing-dktp-contact/test_cases/reports"
files = ["log.html", "report.html"]

email_user = os.environ.get('DB_USER')
email_password = os.environ.get('DB_PASS')
email_send = 'a.mehairjan@dktp.nl'  # Replace with recipient you want to send

today = date.today()
subject = f'Test Resultaten van {today.strftime("%d/%m/%Y")}'

msg = MIMEMultipart()
msg['From'] = email_user
msg['To'] = email_send
msg['Subject'] = subject

body = f'Geachte meneer/mevrouw, \n \nHier zijn de test resultaten van {today.strftime("%d/%m/%Y")}. \n \n Met ' \
       f'vriendelijke groet, \n \nArvind Mehairjan '
msg.attach(MIMEText(body, 'plain'))

for f in files:
    file_path = os.path.join(dir_path, f)
    attachment = MIMEApplication(open(file_path, "rb").read(), _subtype="txt")
    attachment.add_header('Content-Disposition', 'attachment', filename=f)
    msg.attach(attachment)

text = msg.as_string()
server = smtplib.SMTP('smtp.office365.com', 587)
server.starttls()
server.login(email_user, email_password)

server.sendmail(email_user, email_send, text)
server.quit()
